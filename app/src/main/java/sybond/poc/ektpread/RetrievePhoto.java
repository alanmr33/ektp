package sybond.poc.ektpread;

/**
 * Created by Indocyber on 02/04/2018.
 */

public class RetrievePhoto {
    static {
        System.loadLibrary("CoreExploit");
    }
    public native byte[] GetMutualAuth();
    public native byte[] ReadDemographicData();
    public native byte[] RetrieveData();
    public native byte[] SelectDF();
    public native byte[] SelectEFPhoto();
}

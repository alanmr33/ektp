package com.clumsiam.nfcpoc;

/**
 * Created by Indocyber on 02/04/2018.
 */

public abstract class EKTP {
    public static String getString(byte[] bArr) {
        int length = bArr.length;
        int i = length / 16;
        String str = "";
        String str2 = "%02x";
        for (int i2 = 0; i2 < i + 1; i2++) {
            for (int i3 = i2 * 16; i3 <= ((i2 + 1) * 16) - 1; i3++) {
                if (i3 < length) {
                    str = str.concat(String.format(str2, new Object[]{Byte.valueOf(bArr[i3])}));
                }
            }
        }
        return str;
    }

    public static byte[] getByte(String str) {
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }

    public static boolean isValid(byte[] bArr) {
        return ((bArr[bArr.length + -2] == (byte) -112 ? 1 : 0) & (bArr[bArr.length + -1] == (byte) 0 ? 1 : 0)) != 0;
    }
}

package com.clumsiam.nfcpoc;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;


import java.io.File;
import java.io.FileOutputStream;
import me.echodev.resizer.Resizer;
import sybond.poc.ektpread.RetrievePhoto;

public class MainActivity extends AppCompatActivity {
    private PendingIntent nfcIntent;
    private int KTPCode = 112;
    private NfcAdapter adapter;
    private RetrievePhoto retrievePhoto=new RetrievePhoto();
    static {
        System.loadLibrary("CoreExploit");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        retrievePhoto.ReadDemographicData();
        retrievePhoto.SelectDF();
        retrievePhoto.SelectEFPhoto();
        retrievePhoto.RetrieveData();
        retrievePhoto.GetMutualAuth();
        setContentView(R.layout.activity_main);
        if(adapter==null) {
            adapter = NfcAdapter.getDefaultAdapter(this);
        }
        if (adapter != null) {
            if(!adapter.isEnabled()){
                Toast.makeText(this,"NFC Disabled",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this,"No NFC",Toast.LENGTH_SHORT).show();
        }
        nfcIntent=PendingIntent.getActivity(this, 0,
                (new Intent(this, this.getClass())).addFlags(536870912), 0);
        processIntent(getIntent());
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(this.adapter!=null) {
            if (this.adapter.isEnabled()) {
                IntentFilter[] filters=new IntentFilter[1];
                filters[0]= new IntentFilter();
                filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
                filters[0].addCategory(Intent.CATEGORY_DEFAULT);
                this.adapter.enableForegroundDispatch(MainActivity.this, this.nfcIntent, filters, new String[][]{
                        {Ndef.class.getName()}, {NdefFormatable.class.getName()}
                });
            }
        }

    }
    @Override
    protected void onPause() {
        super.onPause();
        if(this.adapter != null) {
            this.adapter.disableForegroundDispatch(this);
        }

    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i("Action",intent.getAction());
        processIntent(intent);
        Toast.makeText(this,"Card Discovered",Toast.LENGTH_SHORT).show();
    }
    private static byte[] copy(byte[] bArr, byte[] bArr2) {
        int length = bArr.length;
        int length2 = bArr2.length;
        Object obj = new byte[(length + length2)];
        System.arraycopy(bArr, 0, obj, 0, length);
        System.arraycopy(bArr2, 0, obj, length, length2);
        return (byte[]) obj;
    }
    private void processIntent(Intent intent){
        String action = intent.getAction();
        if (this.adapter.isEnabled() && "android.nfc.action.TECH_DISCOVERED".equals(action)) {
            IsoDep isoDep = IsoDep.get((Tag) intent.getParcelableExtra("android.nfc.extra.TAG"));
            try {
                if (isoDep == null) {
                    Toast.makeText(this,"EKTP Not Detected",Toast.LENGTH_SHORT).show();
                } else {
                    isoDep.connect();
                    if (!EKTP.isValid(isoDep.transceive(retrievePhoto.SelectDF()))) {
                        Toast.makeText(this, "Not E-KTP", Toast.LENGTH_SHORT).show();
                    } else if (EKTP.isValid(isoDep.transceive(retrievePhoto.SelectEFPhoto()))) {
                        byte[] transceive = isoDep.transceive(copy(retrievePhoto.RetrieveData(), EKTP.getByte("000008")));
                        if (EKTP.isValid(transceive)) {
                            int i = ((transceive[0] << 8) & 65280) | (transceive[1] & 255);
                            byte[] obj = new byte[i];
                            System.arraycopy(transceive, 2, obj, 0, transceive.length - 4);
                            int i2 = 8;
                            Log.i("EKTP Len","Panjang Byte "+i);
                            while (i2 < i) {
                                byte[] a;
                                byte[] transceive2;
                                if (this.KTPCode + i2 > i) {
                                    a = copy(retrievePhoto.RetrieveData(), EKTP.getByte(String.format("%04X%02X", new Object[]{Integer.valueOf(i2), Integer.valueOf((i - i2) + 2)})));
                                    transceive2 = isoDep.transceive(a);
                                    if (EKTP.isValid(transceive2)) {
                                        System.arraycopy(transceive2, 0, obj, i2 - 2, transceive2.length - 2);
                                    } else {
                                        throw new Exception("Error APDU: " + EKTP.getString((a)));
                                    }
                                }
                                a = copy(retrievePhoto.RetrieveData(), EKTP.getByte(String.format("%04X%02X", new Object[]{Integer.valueOf(i2), Integer.valueOf(this.KTPCode)})));
                                transceive2 = isoDep.transceive(a);
                                if (EKTP.isValid(transceive2)) {
                                    if(((transceive2.length - 2)+(i2 - 2))<=obj.length) {
                                        System.arraycopy(transceive2, 0, obj, i2 - 2, transceive2.length - 2);
                                    }
                                } else {
                                    throw new Exception("Error APDU: " + EKTP.getString(a));
                                }
                                i2 += this.KTPCode;
                            }
                            File file=FileCreator.createFile(this,"tmp");
                            FileOutputStream outputStream=new FileOutputStream(file);
                            outputStream.write(obj);
                            outputStream.close();
                            ImageView imageView = findViewById(R.id.imageViewPhoto);
                            Bitmap resizedImage = new Resizer(this)
                                    .setTargetLength(1080)
                                    .setSourceImage(file)
                                    .getResizedBitmap();
                            new BitmapFactory.Options().inJustDecodeBounds = true;
                            imageView.setImageBitmap(resizedImage);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this,"Gagal Membaca Gambar KTP ",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this,"Silahkan Tempelkan KTP",Toast.LENGTH_SHORT).show();
        }
    }
}

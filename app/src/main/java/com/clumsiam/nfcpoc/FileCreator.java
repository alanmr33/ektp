package com.clumsiam.nfcpoc;

import android.app.Activity;
import android.content.Context;

import java.io.File;
import java.io.IOException;

/**
 * Created by Indocyber on 04/04/2018.
 */

public abstract class FileCreator {
    public static final File createFile(Context ctx, String fileName){
        String ext=".jpg";
        File storageDir = ctx.getExternalFilesDir("temp");
        File file = null;
        try {
            if(!storageDir.exists()){
                storageDir.exists();
            }
            file = new File(storageDir.getAbsolutePath()+"/"+fileName+ext);
            if(!file.exists()){
                file.createNewFile();
            }
        } catch (IOException e) {
            e.getStackTrace();
        }
        return file;
    }

}
